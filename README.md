# POM Demo

This project is a direct, if simple, example of a POM and POM Inheritance.

## Basics

![Maven inheritance](resources/inheritance.png)

## Execution

Review each POM, `pom.xml` and `parent-pom.xml`. Note how the Parent provides and the Project can override properties.

### Effective POM

`mvn help:effective-pom` 

Note how the Super POM and parent POM influence the project's POM. Note which properties are inherited from the Maven Super POM and the `parent-pom.xml`.

`mvn -f parent-pom.xml help:effective-pom`

Note how the Super POM influences the **parent** POM.

## JHipster Yeoman and Blueprint Support

Details of how JHipster [uses and extends Yeoman are found in the documentation](https://www.jhipster.tech/modules/extending-and-customizing/).

Kinsale can extend JHipster with public and private modules and blueprints.

JHipster blueprints can be local, or published in an NPM repository.

## Glossary

- POM &mdash; Project Object Model
- yeoman &mdash; a way to prescribe best practices and tools to help teams stay productive. To do so, yeoman provides a generator ecosystem. A yeoman generator is a plugin that can be run with the `yo` command to scaffold complete projects or useful parts of a project.
- JHipster &mdash; [JHipster](https://jhipster.tech/) is a development platform to quickly generate, develop, & deploy modern web applications & microservice architectures.

